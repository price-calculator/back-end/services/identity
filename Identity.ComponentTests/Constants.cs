namespace Identity.ComponentTest;

public static class Constants
{
    public const string USER_FIRSTNAME = "John";
    public const string USER_LASTNAME = "Doe";
    public const string USER_USERNAME = "john.doe";
    public const string USER_EMAIL = "john.doe@abc.com";
    public const string USER_PASSWORD = "Pa$$word123";
}
