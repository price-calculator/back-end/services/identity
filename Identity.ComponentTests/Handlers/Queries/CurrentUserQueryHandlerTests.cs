namespace Identity.ComponentTest.Handlers.Queries;

using System.Threading.Tasks;

using Application.Exceptions;
using Application.Handlers.Commands;
using Application.Handlers.Queries;

using NUnit.Framework;

using Shouldly;

using static Constants;
using static Testing;

[TestFixture]
public class CurrentUserQueryHandlerTests : TestBase
{
    [Test]
    public async Task Handle_HappyFlow_GetsCurrentUser()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = USER_FIRSTNAME,
            LastName = USER_LASTNAME,
            Username = USER_USERNAME,
            Email = USER_EMAIL,
            Password = USER_PASSWORD,
        };
        await SendAsync(registerCommand);

        var query = new CurrentUserQuery();

        // Act
        var user = await SendAsync(query);

        // Assert
        user.ShouldNotBeNull();
        user.FirstName.ShouldBe(registerCommand.FirstName);
        user.LastName.ShouldBe(registerCommand.LastName);
        user.Token.ShouldNotBeNull();
    }

    [Test]
    public async Task Handle_UserDoesNotExists_ThrowsException()
    {
        // Arrange
        var query = new CurrentUserQuery();

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => SendAsync(query));
    }
}
