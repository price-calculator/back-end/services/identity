pipeline {
    agent {
        docker { image 'mcr.microsoft.com/dotnet/sdk:6.0-alpine' }
    }
    environment {
        DOTNET_CLI_HOME = "/tmp/DOTNET_CLI_HOME"
        IGNORE_NORMALISATION_GIT_HEAD_MOVE = 1
        NEXUS_NUGET_API_KEY = credentials('nexus-nuget-api-key')
        NEXUS_USERNAME = credentials('nexus-username')
        NEXUS_PASSWORD = credentials('nexus-password')
    }
    stages {
        stage('Build') {
            steps {
                sh 'dotnet nuget add source $NEXUS_NUGET_URL/nuget-group/index.json -n nuget-group -u $NEXUS_USERNAME -p $NEXUS_PASSWORD --store-password-in-clear-text' 
                sh 'dotnet nuget disable source nuget.org'
                sh 'dotnet clean'
                sh 'dotnet build --configuration Release'
            }
        }
        stage('Test') {
            steps {
                sh 'dotnet nuget add source $NEXUS_NUGET_URL/nuget-group/index.json -n nuget-group -u $NEXUS_USERNAME -p $NEXUS_PASSWORD --store-password-in-clear-text' 
                sh 'dotnet nuget disable source nuget.org'
                sh 'cd *.UnitTests'
                sh 'dotnet test -l:trx;LogFileName=TestResults.trx'
            }
        }
        stage('Publish') {
            steps {
                sh 'dotnet tool install --global GitVersion.Tool --version 5.10.1'
                sh '$DOTNET_CLI_HOME/.dotnet/tools/dotnet-gitversion /output buildserver'
                script {
                    def branchName = "master";
                    def buildNumber = "0";
                    def semVersion = "0.1.0";
                    
                    def filePath = readFile "${WORKSPACE}/gitversion.properties";
                    def lines = filePath.readLines();
                    
                    for (line in lines) {
                        def (name, value) = line.tokenize('=');
                        switch(name) {
                            case "GitVersion_BranchName":
                                branchName = value;
                                break;
                            case "GitVersion_BuildMetaData":
                                buildNumber = value;
                                break;
                            case "GitVersion_SemVer":
                                semVersion = value;
                                break;
                        }
                    }
                    
                    if (branchName.contains("feature")) {
                        def (feature, code) = branchName.tokenize('/');
                        env.SEMANTIC_VERSION = buildNumber != null ? "${semVersion}-alpha-${code}-${buildNumber}" : "${semVersion}-alpha-${code}-0";
                        return;
                    }
                    
                    if (buildNumber != null) {
                        env.SEMANTIC_VERSION = "${semVersion}-preview-${buildNumber}";
                        return;
                    }
                    
                    env.SEMANTIC_VERSION = "${semVersion}";
                }
                sh 'docker build . -t $IMAGE_NAME:$SEMANTIC_VERSION -f *.API/Dockerfile --build-arg NEXUS_URL=http://{IP}:{PORT}}/repository/nuget-group/index.json --build-arg NEXUS_USERNAME={NEXUS_USERNAME} --build-arg NEXUS_PASSWORD={NEXUS_PASSWORD}' 
            }
        }
    }
}