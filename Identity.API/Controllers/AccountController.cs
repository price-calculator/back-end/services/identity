namespace Identity.API.Controllers;

using System.Net;
using System.Threading.Tasks;

using Application.Handlers.Commands;
using Application.Handlers.Queries;
using Application.Model;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

public class AccountController : ControllerBaseCustom
{
    public AccountController(IMediator mediator) : base(mediator)
    {
    }

    [AllowAnonymous]
    [HttpPost("login")]
    [ProducesResponseType(typeof(User), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<User>> Login([FromBody] LoginCommand command)
    {
        return await Mediator.Send(command);
    }

    [AllowAnonymous]
    [HttpPost("register")]
    [ProducesResponseType(typeof(User), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<User>> Register([FromBody] RegisterCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpGet]
    [ProducesResponseType(typeof(User), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<User>> CurrentUser()
    {
        return await Mediator.Send(new CurrentUserQuery());
    }
}
