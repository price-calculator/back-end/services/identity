namespace Identity.Application.Model;

public class User
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Token { get; set; }
}
