namespace Identity.Application.Handlers.Queries;

using System.Net;
using System.Threading;

using System.Threading.Tasks;

using AutoMapper;

using Domain.Models;

using Exceptions;

using MediatR;

using Microsoft.AspNetCore.Identity;

using Model;

using UserAccessors;

public class CurrentUserQueryHandler : IRequestHandler<CurrentUserQuery, User>
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IUserAccessor _userAccessor;
    private readonly IMapper _mapper;

    public CurrentUserQueryHandler(UserManager<ApplicationUser> userManager, IUserAccessor userAccessor, IMapper mapper)
    {
        _userManager = userManager;
        _userAccessor = userAccessor;
        _mapper = mapper;
    }

    public async Task<User> Handle(CurrentUserQuery request, CancellationToken cancellationToken)
    {
        var token = _userAccessor.GetCurrentToken();
        var userName = _userAccessor.GetCurrentUsername();
        if (string.IsNullOrEmpty(userName))
        {
            throw new AccountException(HttpStatusCode.BadRequest, new { message = "User not logged in" });
        }

        var applicationUser = await _userManager.FindByNameAsync(userName);
        if (applicationUser != null)
        {
            var user = _mapper.Map<ApplicationUser, User>(applicationUser);
            user.Token = token;
            return user;
        }

        throw new AccountException(HttpStatusCode.BadRequest, new { message = "User not logged in" });
    }
}
