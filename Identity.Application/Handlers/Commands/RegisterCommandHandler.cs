namespace Identity.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using Domain.Models;

using Exceptions;

using JwtGenerators;

using MediatR;

using Microsoft.AspNetCore.Identity;

using Model;

public class RegisterCommandHandler : IRequestHandler<RegisterCommand, User>
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IJwtGenerator _jwtGenerator;
    private readonly IMapper _mapper;

    public RegisterCommandHandler(UserManager<ApplicationUser> userManager, IJwtGenerator jwtGenerator, IMapper mapper)
    {
        _userManager = userManager;
        _jwtGenerator = jwtGenerator;
        _mapper = mapper;
    }

    public async Task<User> Handle(RegisterCommand request, CancellationToken cancellationToken)
    {
        if ((await _userManager.FindByEmailAsync(request.Email)) != null)
        {
            throw new AccountException
            (
                HttpStatusCode.BadRequest,
                new { message = "Email already exists" }
            );
        }

        if ((await _userManager.FindByNameAsync(request.Username)) != null)
        {
            throw new AccountException
            (
                HttpStatusCode.BadRequest,
                new { message = "UserName already exists" }
            );
        }

        var applicationUser = _mapper.Map<RegisterCommand, ApplicationUser>(request);
        var identityResult = await _userManager.CreateAsync(applicationUser, request.Password);
        if (identityResult.Succeeded)
        {
            var token = _jwtGenerator.CreateToken(applicationUser);
            var user = _mapper.Map<ApplicationUser, User>(applicationUser);
            user.Token = token;
            return user;
        }

        throw new Exception("Problem creating user");
    }
}
