namespace Identity.Application.Handlers.Commands;

using MediatR;

using Model;

public class LoginCommand : IRequest<User>
{
    public string Email { get; set; }
    public string Password { get; set; }
}
