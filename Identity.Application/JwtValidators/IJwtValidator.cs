namespace Identity.Application.JwtValidators;

public interface IJwtValidator
{
    bool ValidateToken(string token);
}
