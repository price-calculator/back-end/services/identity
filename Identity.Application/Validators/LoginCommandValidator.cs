namespace Identity.Application.Validators;

using FluentValidation;

using Handlers.Commands;

public class LoginCommandValidator : AbstractValidator<LoginCommand>
{
    public LoginCommandValidator()
    {
        RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Email must be valid.");
        RuleFor(x => x.Password).NotEmpty().WithMessage("You must supply a password.");
    }
}
