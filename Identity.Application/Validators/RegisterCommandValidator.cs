namespace Identity.Application.Validators;

using FluentValidation;

using Handlers.Commands;

public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
{
    public RegisterCommandValidator()
    {
        RuleFor(x => x.FirstName).NotEmpty();
        RuleFor(x => x.LastName).NotEmpty();
        RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Email must be valid.");
        RuleFor(x => x.Password).Password();
    }
}
