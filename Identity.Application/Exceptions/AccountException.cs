namespace Identity.Application.Exceptions;

using System;
using System.Net;

public class AccountException : Exception
{
    public readonly HttpStatusCode Code;
    public readonly object Error;

    public AccountException(HttpStatusCode code, object error = null)
    {
        Code = code;
        Error = error;
    }
}
