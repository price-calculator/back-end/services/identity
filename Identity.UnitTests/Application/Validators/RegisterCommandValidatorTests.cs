namespace Identity.UnitTests.Application.Validators;

using AutoFixture;

using FluentValidation.TestHelper;

using Identity.Application.Handlers.Commands;
using Identity.Application.Validators;

using NUnit.Framework;

[TestFixture]
public class RegisterCommandValidatorTests
{
    private const string VALID_EMAIL = "john.doe@abc.com";
    private const string VALID_PASSWORD = "Pa$$word123";
    private readonly Fixture _fixture = new();
    private RegisterCommandValidator _validator;

    [SetUp]
    public void SetUp() => _validator = new RegisterCommandValidator();

    [Test]
    public void RegisterCommandValidator_ValidRegisterCommand_Success()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithEmptyFirstName_Fail()
    {
        // Arrange

        var registerCommand = new RegisterCommand
        {
            FirstName = string.Empty,
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithNullFirstName_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = null,
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithEmptyLastName_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = string.Empty,
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithNullLastName_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = null,
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithEmptyEmail_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = string.Empty,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithNullEmail_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = null,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithIncorrectEmail_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = _fixture.Create<string>(),
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithEmptyPassword_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = string.Empty,
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void RegisterCommandValidator_RegisterCommandWithNullPassword_Fail()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = _fixture.Create<string>(),
            LastName = _fixture.Create<string>(),
            Username = _fixture.Create<string>(),
            Email = VALID_EMAIL,
            Password = null,
        };

        // Act
        var result = _validator.TestValidate(registerCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.FirstName);
        result.ShouldNotHaveValidationErrorFor(c => c.LastName);
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldHaveValidationErrorFor(c => c.Password);
    }
}
