namespace Identity.UnitTests.Application.Handlers.Queries;

using System;
using System.Threading.Tasks;

using AutoFixture;

using AutoMapper;

using Identity.Application.Exceptions;
using Identity.Application.Handlers.Queries;
using Identity.Application.Model;
using Identity.Application.UserAccessors;
using Identity.Domain.Models;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class CurrentUserQueryHandlerTests
{
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Mock<IMapper> _mapperMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private Mock<UserManager<ApplicationUser>> _userManagerMock;
    private CurrentUserQueryHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _userManagerMock = new(
            new Mock<IUserStore<ApplicationUser>>(MockBehavior.Loose).Object,
            new Mock<IOptions<IdentityOptions>>(MockBehavior.Loose).Object,
            new Mock<IPasswordHasher<ApplicationUser>>(MockBehavior.Loose).Object,
            Array.Empty<IUserValidator<ApplicationUser>>(),
            Array.Empty<IPasswordValidator<ApplicationUser>>(),
            new Mock<ILookupNormalizer>(MockBehavior.Loose).Object,
            new Mock<IdentityErrorDescriber>(MockBehavior.Loose).Object,
            new Mock<IServiceProvider>(MockBehavior.Loose).Object,
            new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _userManagerMock.SetupProperty(x => x.Logger, new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _sut = new CurrentUserQueryHandler(_userManagerMock.Object, _userAccessorMock.Object, _mapperMock.Object);
    }
    [Test]
    public async Task Handler_HappyFlow_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var username = _fixture.Create<string>();
        var currentUserQuery = new CurrentUserQuery();
        var applicationUser = _fixture.Create<ApplicationUser>();
        applicationUser.UserName = username;
        var user = new User
        {
            FirstName = applicationUser.FirstName,
            LastName = applicationUser.LastName,
            Token = token
        };

        _userAccessorMock.Setup(x => x.GetCurrentToken()).Returns(token);
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _userManagerMock.Setup(x => x.FindByNameAsync(username)).ReturnsAsync(applicationUser);

        _mapperMock.Setup(x => x.Map<ApplicationUser, User>(applicationUser)).Returns(user);

        // Act
        var result = await _sut.Handle(currentUserQuery, default);

        // Assert
        result.ShouldSatisfyAllConditions(
            () => result.FirstName.ShouldBe(user.FirstName),
            () => result.LastName.ShouldBe(user.LastName),
            () => result.Token.ShouldBe(token));

        _userAccessorMock.VerifyAll();
        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
    }

    [Test]
    public async Task Handler_CouldNotGetCurrentUserFromHttpContext_Fail()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var username = _fixture.Create<string>();
        var currentUserQuery = new CurrentUserQuery();

        _userAccessorMock.Setup(x => x.GetCurrentToken()).Returns(token);
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(string.Empty);

        // Act/Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(currentUserQuery, default));

        _userAccessorMock.VerifyAll();
        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
    }

    [Test]
    public async Task Handler_UsernameDoesNotExists_Fail()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var username = _fixture.Create<string>();
        var currentUserQuery = new CurrentUserQuery();
        var applicationUser = _fixture.Create<ApplicationUser>();
        applicationUser.UserName = username;
        var user = new User
        {
            FirstName = applicationUser.FirstName,
            LastName = applicationUser.LastName,
            Token = token
        };

        _userAccessorMock.Setup(x => x.GetCurrentToken()).Returns(token);
        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _userManagerMock.Setup(x => x.FindByNameAsync(username)).ReturnsAsync((ApplicationUser)null);

        // Act/Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(currentUserQuery, default));

        _userAccessorMock.VerifyAll();
        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
    }
}
