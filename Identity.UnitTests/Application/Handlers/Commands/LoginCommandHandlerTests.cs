namespace Identity.UnitTests.Application.Handlers.Commands;

using System;
using System.Threading.Tasks;

using AutoFixture;

using AutoMapper;

using Identity.Application.Exceptions;
using Identity.Application.Handlers.Commands;
using Identity.Application.JwtGenerators;
using Identity.Application.Model;

using Identity.Domain.Models;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class LoginCommandHandlerTests
{
    private readonly Mock<IJwtGenerator> _jwtGeneratorMock = new(MockBehavior.Strict);
    private readonly Mock<IMapper> _mapperMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private Mock<UserManager<ApplicationUser>> _userManagerMock;
    private Mock<SignInManager<ApplicationUser>> _signInManagerMock;
    private LoginCommandHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _userManagerMock = new(behavior: MockBehavior.Strict,
            new Mock<IUserStore<ApplicationUser>>(MockBehavior.Loose).Object,
            new Mock<IOptions<IdentityOptions>>(MockBehavior.Loose).Object,
            new Mock<IPasswordHasher<ApplicationUser>>(MockBehavior.Loose).Object,
            Array.Empty<IUserValidator<ApplicationUser>>(),
            Array.Empty<IPasswordValidator<ApplicationUser>>(),
            new Mock<ILookupNormalizer>(MockBehavior.Loose).Object,
            new Mock<IdentityErrorDescriber>(MockBehavior.Loose).Object,
            new Mock<IServiceProvider>(MockBehavior.Loose).Object,
            new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _userManagerMock.SetupProperty(x => x.Logger, new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _signInManagerMock = new(behavior: MockBehavior.Strict,
            _userManagerMock.Object,
            new Mock<IHttpContextAccessor>(MockBehavior.Loose).Object,
            new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>(MockBehavior.Loose).Object,
            new Mock<IOptions<IdentityOptions>>(MockBehavior.Loose).Object,
            new Mock<ILogger<SignInManager<ApplicationUser>>>(MockBehavior.Loose).Object,
            new Mock<IAuthenticationSchemeProvider>(MockBehavior.Loose).Object,
            new Mock<IUserConfirmation<ApplicationUser>>(MockBehavior.Loose).Object);

        _signInManagerMock.SetupProperty(x => x.Logger, new Mock<ILogger<SignInManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _sut = new LoginCommandHandler(_userManagerMock.Object, _signInManagerMock.Object, _jwtGeneratorMock.Object, _mapperMock.Object);
    }

    [Test]
    public async Task Handler_HappyFlow_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var loginCommand = _fixture.Create<LoginCommand>();
        var applicationUser = _fixture.Create<ApplicationUser>();
        applicationUser.Email = loginCommand.Email;
        var user = new User
        {
            FirstName = applicationUser.FirstName,
            LastName = applicationUser.LastName,
            Token = token
        };

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(loginCommand.Email))
            .ReturnsAsync(applicationUser);

        _signInManagerMock
            .Setup(x => x.CheckPasswordSignInAsync(applicationUser, loginCommand.Password, false))
            .ReturnsAsync(SignInResult.Success);

        _mapperMock
            .Setup(o => o.Map<ApplicationUser, User>(applicationUser))
            .Returns(new User
            {
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                Token = token
            });

        _jwtGeneratorMock
            .Setup(x => x.CreateToken(applicationUser))
            .Returns(token);

        // Act
        var result = await _sut.Handle(loginCommand, default);

        // Assert
        result.ShouldSatisfyAllConditions(
            () => result.FirstName.ShouldBe(user.FirstName),
            () => result.LastName.ShouldBe(user.LastName),
            () => result.Token.ShouldBe(token));

        _userManagerMock.VerifyAll();
        _signInManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }

    [Test]
    public async Task Handler_EmailDoesNotExists_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var loginCommand = _fixture.Create<LoginCommand>();

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(loginCommand.Email))
            .ReturnsAsync((ApplicationUser)null);

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(loginCommand, default));

        _userManagerMock.VerifyAll();
        _signInManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }

    [Test]
    public async Task Handler_CheckPasswordFails_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var loginCommand = _fixture.Create<LoginCommand>();
        var applicationUser = _fixture.Create<ApplicationUser>();
        applicationUser.Email = loginCommand.Email;

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(loginCommand.Email))
            .ReturnsAsync(applicationUser);

        _signInManagerMock
            .Setup(x => x.CheckPasswordSignInAsync(applicationUser, loginCommand.Password, false))
            .ReturnsAsync(SignInResult.Failed);

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(loginCommand, default));

        _userManagerMock.VerifyAll();
        _signInManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }
}
