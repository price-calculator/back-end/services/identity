namespace Identity.UnitTests.Infrastructure.JwtValidators;

using System;

using AutoFixture;

using Identity.Domain.Models;
using Identity.Infrastructure.JwtGenerators;
using Identity.Infrastructure.JwtValidators;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class JwtValidatorsTests
{
    [SetUp]
    public void SetUp() => Environment.SetEnvironmentVariable("JWT_KEY", "ThisIsTheSecretJwt");

    [TearDown]
    public void TearDown() => Environment.SetEnvironmentVariable("JWT_KEY", null);

    [Test]
    public void ValidateToken_Token_Success()
    {
        // Arrange
        var fixture = new Fixture();
        var user = fixture.Create<ApplicationUser>();
        var token = new JwtGenerator().CreateToken(user);

        // Act
        var result = new JwtValidator().ValidateToken(token);

        // Assert
        result.ShouldBeTrue();
    }
}
