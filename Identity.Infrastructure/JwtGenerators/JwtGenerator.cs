namespace Identity.Infrastructure.JwtGenerators;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using Application.JwtGenerators;

using Domain.Models;

using Microsoft.IdentityModel.Tokens;

public class JwtGenerator : IJwtGenerator
{
    private readonly SymmetricSecurityKey _key;

    public JwtGenerator()
    {
        var jwtKey = Environment.GetEnvironmentVariable("JWT_KEY");
        _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
    }

    public string CreateToken(ApplicationUser user)
    {
        var claims = new List<Claim>
        {
            new Claim(JwtRegisteredClaimNames.NameId, user.UserName),
        };

        var signingCredentials = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature);

        var securityTokenDescriptor = new SecurityTokenDescriptor()
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.Now.AddDays(7),
            SigningCredentials = signingCredentials
        };

        var securityTokenHandler = new JwtSecurityTokenHandler();
        var token = securityTokenHandler.CreateToken(securityTokenDescriptor);

        return securityTokenHandler.WriteToken(token);
    }
}
