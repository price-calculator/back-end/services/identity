namespace Identity.Infrastructure.JwtValidators;

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

using Application.JwtValidators;

using AutoMapper.Configuration;

using Microsoft.IdentityModel.Tokens;

public class JwtValidator : IJwtValidator
{
    private readonly SymmetricSecurityKey _key;

    public JwtValidator()
    {
        var jwtKey = Environment.GetEnvironmentVariable("JWT_KEY");
        _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
    }
    public bool ValidateToken(string token)
    {
        var securityTokenHandler = new JwtSecurityTokenHandler();
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = _key,
            ValidateAudience = false,
            ValidateIssuer = false,
        };

        var claims = securityTokenHandler.ValidateToken(token, tokenValidationParameters, out _);

        return claims.Claims != null && claims.Claims.Any();
    }
}
